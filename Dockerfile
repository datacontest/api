FROM python:3.6


WORKDIR /app
ADD requirements/ requirements/
RUN pip install -r requirements/dev.txt

ADD . /app
