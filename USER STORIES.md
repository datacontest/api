User Stories
============

A first definition of user stories for the MVP.

Anonymous User
--------------

* As an anonymous user, i want to register to the platform to become a user
  * I want to use my github account to register
* As an anonymous user, i want to login

User
----

* As a user, i want to see a list of datathons
  * Only public datathons are shown
* As a user, i want to see the detail of a datathon
  * A title and a description of the problem we are attacking
  * The training and test datasets, to see the data
* As a user, i want to create a datathon
  * An evaluation metric is defined
* As a user, i want to join a datathon

Organizer
---------

* As an organizer, i want to add a training set, a test set and a validation set
  * CSVs with headers are expected
  * A header is specified as the variable to be predicted
  * The validation set is only visible to the organizer during the contest
  * (FUTURE) Optionally, upload only a file, to be converted into a the different datasets. I want to be able to specify the porcentage distribution of the splits
* As an organizer, i want to mark a contest as ended to finish it
  * Submissions will be closed from this moment

Participant
-----------

* As a participant, i want to submit a solution to be scored
  * During the contest, the submission is evaluated against the (public) test set
  * When the datathon is marked as ended by the organizer, a final evaluation is performed against the (private) validation set
* As a participant (of an open datathon), i want to view a history of my submissions
* As a participant (of an open datathon), i want to select which of my submissions will be finally used against the validation set
* As a participant, i want to see a leaderboard showing me the participants classification
  * During the contest the score of each participant is just its best historical submission (evaluated against the test set)
  * When the datathon is closed, the submission choosen by the participant is evaluated against the validation set
  * Once the datathon is closed, the "up and down scaling" in the leaderboard (the difference between the position at the end of the contest respect to the final position) is shown

=== Vocabulary

* **User**: A logged user.
* **Public datathon**: A public datathon is a datathon with an specified dataset and evaluation metric.
* **Organizer**: A user is the organizer of the datathons she created.
* **Participant**: A user can join a public datathon (while it's open), becoming a participant. A participant can send submissions to a datathon during the contest.
