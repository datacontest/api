from django.core.files.storage import FileSystemStorage
from django.db import models

fs = FileSystemStorage()


class Dataset(models.Model):
    description = models.TextField()
    training_set = models.FileField(storage=fs)
    test_set = models.FileField(storage=fs)
    validation_set = models.FileField(storage=fs)
    target_variable_name = models.CharField(max_length=64)


class Datathon(models.Model):
    AUC = 'AUC'
    ACCURACY = 'ACCURACY'
    MSE = 'MSE'
    MAE = 'MAE'

    EVALUATION_METRIC_CHOICES = (
        (AUC, 'Area Under the ROC Curve'),
        (ACCURACY, 'Accuracy'),
        (MSE, 'Mean Squared Error'),
        (MAE, 'Mean Absolute Error'),
    )

    created_at = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=100)
    description = models.TextField()
    metric = models.CharField(max_length=16,
                              choices=EVALUATION_METRIC_CHOICES,
                              default=AUC)
    organizer = models.ForeignKey('auth.User',
                                  related_name='datathons_organized',
                                  on_delete=models.CASCADE)
    dataset = models.OneToOneField(Dataset,
                                   on_delete=models.CASCADE)
    participants = models.ManyToManyField('auth.User')

    class Meta:
        ordering = ('created_at',)

    def is_classification_problem(self):
        return self.metric in (self.AUC, self.ACCURACY)

    def is_regression_problem(self):
        return self.metric in (self.MAE, self.MSE)


class Submission(models.Model):
    STATUS_PENDING = 'PENDING'
    STATUS_PROCESSING = 'PROCESSING'
    STATUS_EVALUATED = 'EVALUATED'

    STATUS_CHOICES = (
        (STATUS_PENDING, 'Pending'),
        (STATUS_PROCESSING, 'Processing'),
        (STATUS_EVALUATED, 'Evaluated'),
    )

    datathon = models.ForeignKey(Datathon,
                                 on_delete=models.CASCADE)
    participant = models.ForeignKey('auth.User',
                                    on_delete=models.CASCADE)
    submission_file = models.FileField(storage=fs, upload_to='submissions/')
    status = models.CharField(max_length=16,
                              choices=STATUS_CHOICES,
                              default=STATUS_PENDING)
    score = models.DecimalField(max_digits=32, null=True, blank=True, decimal_places=4)
