from rest_framework import permissions


class IsOrganizerOrReadOnly(permissions.BasePermission):
    """
    Custom permission to only allow organizers of an object to edit it.
    """

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True

        # Write permissions are only allowed to the organizer of the datathon.
        return obj.organizer == request.user


class IsRelatedDatathonOrganizerOrReadOnly(permissions.BasePermission):
    """
    Custom permission for datasets.

    Only allow the organizer of the related datathon to edit it.
    """

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True

        return obj.datathon.organizer == request.user


class IsParticipantOrOrganizerOrReadOnly(permissions.BasePermission):
    """
    Custom permission for datathon submissions.
    """
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True

        datathon = obj.datathon
        is_organizer = datathon.organizer == request.user
        is_participant = datathon.participants.filter(pk=request.user.pk).exists()

        return is_organizer or is_participant
