from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.reverse import reverse

from datathons.models import Datathon, Dataset, Submission


class UserSerializer(serializers.HyperlinkedModelSerializer):
    datathons_organized = serializers.HyperlinkedRelatedField(
        many=True,
        view_name='datathon-detail',
        read_only=True,
    )

    class Meta:
        model = User
        fields = ('url', 'id', 'username', 'datathons_organized')


class ParticipantSerializer(serializers.HyperlinkedModelSerializer):
    username = serializers.ReadOnlyField()

    class Meta:
        model = User
        fields = ('url', 'username')


class DatasetSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Dataset
        fields = (
            'url', 'id', 'description', 'target_variable_name',
            'training_set', 'test_set', 'validation_set',
        )


class DatathonSerializer(serializers.HyperlinkedModelSerializer):
    organizer = serializers.ReadOnlyField(source='organizer.username')
    participants = ParticipantSerializer(many=True, read_only=True)
    submissions = serializers.SerializerMethodField()
    dataset = DatasetSerializer()

    class Meta:
        model = Datathon
        fields = (
            'url', 'id', 'organizer', 'title',
            'description', 'metric', 'created_at',
            'dataset', 'participants', 'submissions',
        )

    def create(self, validated_data):
        dataset_data = validated_data.pop('dataset')
        dataset = Dataset.objects.create(**dataset_data)
        return Datathon.objects.create(dataset=dataset, **validated_data)

    def get_participants(self, obj):
        return reverse('datathon-participants', args=[obj.pk], request=self.context['request'])

    def get_submissions(self, obj):
        return reverse('datathon-submissions', args=[obj.pk], request=self.context['request'])


class SubmissionSerializer(serializers.ModelSerializer):
    participant = serializers.ReadOnlyField(source='participant.username')
    status = serializers.ReadOnlyField()
    score = serializers.ReadOnlyField()
    datathon = serializers.ReadOnlyField(source='datathon.url')

    class Meta:
        model = Submission
        fields = (
            'submission_file', 'participant', 'status', 'score', 'datathon',
        )
