from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from datathons import views

urlpatterns = [
    path('', views.api_root),

    path('datathons/', views.DatathonList.as_view(), name='datathons-list'),
    path('datathons/<int:pk>/',
         views.DatathonDetail.as_view(), name='datathon-detail'),
    path('datathons/<int:pk>/participants/',
         views.DatathonParticipants.as_view(), name='datathon-participants'),
    path('datathons/<int:pk>/submissions/',
         views.DatathonSubmissionList.as_view(), name='datathon-submissions'),

    path('users/', views.UserList.as_view(), name='user-list'),
    path('users/<int:pk>/', views.UserDetail.as_view(), name='user-detail'),

    path('datasets/<int:pk>/', views.DatasetDetail.as_view(), name='dataset-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
