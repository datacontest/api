import django_filters.rest_framework
from django.contrib.auth.models import User
from django.http import Http404
from rest_framework import generics
from rest_framework import permissions
from rest_framework import status
from rest_framework.exceptions import ValidationError
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView

from datathons.models import Datathon, Dataset
from datathons.serializers import DatathonSerializer
from datathons.serializers import UserSerializer
from datathons.serializers import DatasetSerializer
from datathons.serializers import SubmissionSerializer
from datathons.serializers import ParticipantSerializer
from datathons.permissions import IsOrganizerOrReadOnly
from datathons.permissions import IsRelatedDatathonOrganizerOrReadOnly
from datathons.permissions import IsParticipantOrOrganizerOrReadOnly


@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'datathons': reverse('datathons-list', request=request, format=format),
    })


class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class DatathonList(generics.ListCreateAPIView):
    queryset = Datathon.objects.all()
    serializer_class = DatathonSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsOrganizerOrReadOnly)

    def perform_create(self, serializer):
        serializer.save(organizer=self.request.user)


class DatathonDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Datathon.objects.all()
    serializer_class = DatathonSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsOrganizerOrReadOnly)


class DatasetDetail(generics.RetrieveUpdateAPIView):
    queryset = Dataset.objects.all()
    serializer_class = DatasetSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsRelatedDatathonOrganizerOrReadOnly)

    # TODO Perform update: should notify all participants of a change in the dataset!


class DatathonSubmissionList(generics.ListCreateAPIView):
    serializer_class = SubmissionSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsParticipantOrOrganizerOrReadOnly)
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend,)

    def get_queryset(self):
        """
        Optionally, restrict the returned submissions to a given user,
        filtering by username.
        """
        try:
            datathon = Datathon.objects.get(pk=self.kwargs['pk'])
            queryset = datathon.submission_set

            username = self.request.query_params.get('username', None)
            if username is not None:
                queryset = queryset.filter(participant__username=username)

            return queryset
        except Datathon.DoesNotExist:
            raise Http404

    def perform_create(self, serializer):
        # I need to replicate the IsParticipantOrOrganizerOrReadOnly permission logic
        # because it's not validated during creation. TODO Think of a better way to do it.
        datathon = Datathon.objects.get(pk=self.kwargs['pk'])

        is_organizer = datathon.organizer == self.request.user
        is_participant = datathon.participants.filter(pk=self.request.user.pk).exists()

        if not (is_organizer or is_participant):
            raise ValidationError('Only participants are allowed to send submissions.')

        serializer.save(datathon_id=self.kwargs['pk'],
                        participant=self.request.user)

        # TODO async task to evaluate the submission


class DatathonParticipants(APIView):
    serializer_class = ParticipantSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def get_datathon(self, pk):
        try:
            return Datathon.objects.get(pk=pk)
        except Datathon.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        datathon = self.get_datathon(pk)
        serializer = ParticipantSerializer(
            datathon.participants,
            context={'request': request},
            many=True,
        )
        return Response(serializer.data)

    def post(self, request, pk, format=None):
        datathon = self.get_datathon(pk)
        datathon.participants.add(request.user)
        datathon.save()

        return Response({}, status=status.HTTP_201_CREATED)

    def delete(self, request, pk, format=None):
        datathon = self.get_datathon(pk)
        datathon.participants.remove(request.user)
        datathon.save()

        return Response({}, status=status.HTTP_204_NO_CONTENT)
